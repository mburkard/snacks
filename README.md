# Snacks

Snacks is a wrapper around [pika](https://pypi.org/project/pika/) to provide a
convenient interface for RabbitMQ.

## Installation
Snacks is available on PyPI and can be installed with:
```shell
pip install snacks
```

## Configuration
```python
from snacks.rabbit import RabbitClient

# These are all the default values if none are provided.
rabbit = RabbitClient(
    host='localhost',
    port=5672,
    default_exchange='',
    virtual_host='/',
    username='guest',
    password='guest'
)
```

## Consume From Queues
To decorate a function as a consumer for a queue use the `consumer` decorator.
```python
@rabbit.consumer('queue.name')
def listen(event: str) -> None:
    print(f'Received request: {event}')
```


## Consume From Generated Queues
To decorate a function as a consumer for a generated queue using given routing
keys use the `listener` decorator.
```python
@rabbit.listener('routing_key')
def listen(event: str) -> None:
    print(f'Received request: {event}')
```

### RPC
To make a decorated function a remote procedure add a return.
```python
@rabbit.listener('routing_key')
def listen(event: str) -> str:
    print(f'Received request: {event}')
    return 'Remote response.'
```

## Full Example

```python
from snacks.rabbit import RabbitClient

# Setup
rabbit = RabbitClient(default_exchange='snacks')
queue = 'snacks'
key = 'snackey'
rabbit.exchange_declare(exchange_type='topic', durable=True)
rabbit.queue_declare(queue=queue, durable=True)
rabbit.queue_bind(queue=queue, routing_key=key)


@rabbit.consumer([queue])
def listen(event: str) -> str:
    print(f'Received request: {event}')
    return 'Rabbits and pikas are snacks.'


if __name__ == '__main__':
    r = rabbit.publish_and_receive(
        'To a python.', key, deserialize=bytes.decode
    )
    print(f'Received response: {r}')
```

Output:
```
Received request: To a python.
Received response: Rabbits and pikas are snacks.
```
